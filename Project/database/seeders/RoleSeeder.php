<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['role_id'=> 1, 'role_name' => 'admin', 'status' => true],
            ['role_id'=> 2, 'role_name' => 'PMO', 'status' => true],
            ['role_id'=> 3, 'role_name' => 'Tech Lead', 'status' => true],
            ['role_id'=> 4, 'role_name' => 'employee', 'status' => true],
            ['role_id'=> 5, 'role_name' => 'mentor', 'status' => true]
        ];

        foreach($roles as $role) {
            Role::create($role);
        }
    }
}
