<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            //
            $table->increments('certificate_id')->length(10);
            
            $table->integer('employee_id')->length(10);
            $table->foreign('employee_id')->references('employee_id')->on('employees')->onDelete('cascade');

            $table->integer('business_unit_id')->length(10)->unsigned();
            $table->foreign('business_unit_id')->references('business_id')->on('business_units')->onDelete('cascade');
            
            $table->date('certificate_start_date');
            $table->date('certificate_end_date');
            $table->string('certificate_file_name')->length(150);
            $table->text('short_description');
            $table->longText('long_description');
            $table->boolean('approval')->default(false);
            $table->dateTime('approved_at');
            $table->boolean('status')->nullable()->default(true);
            $table->softDeletes();
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('certificates', function (Blueprint $table) {
            //
        });
    }
};
