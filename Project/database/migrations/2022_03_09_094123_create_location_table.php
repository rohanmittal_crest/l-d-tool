<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->increments('location_id')->length(10);
            $table->integer('pincode')->length(6);
            $table->decimal('latitude',10, 6);
            $table->decimal('longitude',10, 6);
            $table->string('address')->length('250');
            $table->string('address_1')->length('250');
            $table->string('address_2')->length('250')->nullable();
            $table->softDeletes();
            $table->boolean('status')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *database\migrations\2022_03_09_094123_create_location_table.php
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
};
