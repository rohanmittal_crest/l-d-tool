<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('log_id')->length(10);
            $table->string('log_name')->length(100);
            $table->text('log_description')->length(255);
            $table->integer('subject_id')->length(10);
            $table->string('subject_type')->length(200);
            $table->integer('causer_id')->length(10);
            $table->string('causer_type')->length(200);
            $table->string('properties')->length(255);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
};
