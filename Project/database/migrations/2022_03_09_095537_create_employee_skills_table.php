<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_skills', function (Blueprint $table) {
            $table->increments('skill_id');

            $table->integer('skill_master_id')->length(10)->unsigned();
            $table->foreign('skill_master_id')
                ->references('skill_master_id')
                ->on('skill_master')
                ->onDelete('restrict');

            $table->integer('employee_id')->length(10);
            $table->foreign('employee_id')
                ->references('employee_id')
                ->on('employees')
                ->onDelete('cascade');

            $table->integer('business_id')->length(10)->unsigned();
            $table->foreign('business_id')
                ->references('business_id')
                ->on('business_units')
                ->onDelete('cascade');

            $table->integer('relative_experience')->length(2);
            $table->integer('rating')->length(2);
            $table->dateTime('approved_at');
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *database\migrations\2022_03_09_095537_create_employee_skills_table.php
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_skills');
    }
};
