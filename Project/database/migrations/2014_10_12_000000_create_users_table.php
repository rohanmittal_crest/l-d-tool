<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id')->length(10);
            $table->string('username')->length(100)->unique();
            $table->string('first_name')->length(100);
            $table->string('last_name')->length(100);
            $table->string('avatar')->length(100);

            $table->integer('location_id')->length(10)->unsigned();
            $table->foreign('location_id')->references('location_id')->on('location')->onDelete('cascade');

            $table->bigInteger('mobile_no')->length(10);
            $table->string('email')->length(50)->unique();

            $table->string('password')->length(100);

            $table->integer('business_unit_id')->length(10)->unsigned();
            $table->foreign('business_unit_id')->references('business_id')->on('business_units')->onDelete('cascade');
            
            $table->integer('role_id')->length(10)->unsigned();
            $table->foreign('role_id')->references('role_id')->on('roles')->onDelete('cascade');
            $table->boolean('status')->nullable()->default(true);

            $table->softDeletes();
            //$table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
