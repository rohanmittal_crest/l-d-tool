<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->integer('employee_id')->length(10)->primary();
            //$table->primary('employee_id');

            $table->string('first_name')->length(100);
            $table->string('last_name')->length(100);
            $table->string('email',100)->unique();
            $table->bigInteger('mobile_no')->unsigned()->length(10);

            $table->integer('business_unit')->length(10)->unsigned();
            $table->foreign('business_unit')->references('business_id')->on('business_units')->onDelete('cascade');
            
            $table->integer('direct_manager_id')->length(10);
            
            $table->string('employee_type',100);
            $table->date('date_of_joining');
            
            $table->integer('location_id')->length(10)->unsigned();
            $table->foreign('location_id')->references('location_id')->on('location')->onDelete('cascade');
            
            $table->string('designation',100);
            $table->boolean('status')->default(true);
            $table->string('avatar');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('employees',function (Blueprint $table){
            $table->foreign('direct_manager_id')->references('employee_id')->on('employees')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *database\migrations\2022_03_09_093504_create_employees_table.php
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
